package br.com.treinamento.dojo.service;

import br.com.treinamento.config.AppConfig;
import br.com.treinamento.dojo.model.Comic;
import br.com.treinamento.dojo.model.ComicDataWrapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Objects;

import static org.junit.Assert.*;

/**
 * Created by andreteodoro on 11/03/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppConfig.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MarvelServiceTest {

    @Autowired
    private MarvelService marvelService;

    @Test
    public void testGetComicsByCharacter() throws Exception {
        ComicDataWrapper comicWrapper = marvelService.getComicsByCharacter("Wolverine");
        List<Comic> comics = comicWrapper.getData().getResults();

        Assert.assertTrue(Objects.nonNull(comics));
        Assert.assertTrue(comics.size() > 0);
        Assert.assertTrue(comics.get(0).getDescription().contains("Wolverine"));
        Assert.assertTrue(comics.get(0).getTitle().contains("Wolverine"));
    }

    @Test(expected = Exception.class)
    public void testGetComicsByCharacterNameNull() throws Exception {
        marvelService.getComicsByCharacter(null);
    }
}