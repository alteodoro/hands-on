package br.com.treinamento.dojo.service;

import br.com.treinamento.config.AppConfig;
import br.com.treinamento.dojo.model.Comic;
import br.com.treinamento.dojo.model.ComicDataWrapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by andreteodoro on 11/03/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppConfig.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CacheServiceTest {

    @Autowired
    private MarvelService marvelAPIService;

    @Autowired
    private CacheService cacheService;

    @Test
    public void testGetCacheData() throws Exception {
        marvelAPIService.getComicsByCharacter("Wolverine");

        ConcurrentHashMap<String, ComicDataWrapper> cacheData =
                (ConcurrentHashMap<String, ComicDataWrapper>) cacheService.getCacheData();

        Assert.assertFalse(cacheData.isEmpty());

        List<Comic> cachedComics = cacheData.get("Wolverine").getData().getResults();

        Assert.assertTrue(Objects.nonNull(cachedComics));
        Assert.assertTrue(cachedComics.size() > 0);
        Assert.assertTrue(cachedComics.get(0).getDescription().contains("Wolverine"));
        Assert.assertTrue(cachedComics.get(0).getTitle().contains("Wolverine"));
    }

    @Test
    public void testGetCacheDataWith2Comics() throws Exception {
        marvelAPIService.getComicsByCharacter("Wolverine");
        marvelAPIService.getComicsByCharacter("Ink");

        ConcurrentHashMap<String, ComicDataWrapper> cacheData =
                (ConcurrentHashMap<String, ComicDataWrapper>) cacheService.getCacheData();

        Assert.assertFalse(cacheData.isEmpty());

        List<Comic> wolverineCachedComics = cacheData.get("Wolverine").getData().getResults();
        List<Comic> inkCachedComics = cacheData.get("Ink").getData().getResults();

        Assert.assertTrue(Objects.nonNull(wolverineCachedComics));
        Assert.assertTrue(Objects.nonNull(inkCachedComics));
    }

}