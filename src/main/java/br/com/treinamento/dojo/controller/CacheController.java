package br.com.treinamento.dojo.controller;

import br.com.treinamento.dojo.service.CacheService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by andreteodoro on 11/03/17.
 */
@RestController
public class CacheController {

    @Autowired
    private CacheService cacheService;

    /**
     * Endpoint used to retrieve the comics consolidated in the cache
     *
     * @return cache list
     */
    @RequestMapping(value = "/consolidated", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> listCacheData() throws Exception {
        Object data = cacheService.getCacheData();

        return new ResponseEntity<String>(new Gson().toJson(data), HttpStatus.OK);
    }
}
