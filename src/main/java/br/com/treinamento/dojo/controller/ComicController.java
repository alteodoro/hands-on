package br.com.treinamento.dojo.controller;

import br.com.treinamento.dojo.model.Comic;
import br.com.treinamento.dojo.service.MarvelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

/**
 * Created by andreteodoro on 11/03/17.
 */
@RestController
public class ComicController {

    private MarvelService marvelService;

    @Autowired
    public ComicController(MarvelService marvelService) {
        this.marvelService = marvelService;
    }

    /**
     * Endpoint used to retrieve the comics from a character
     *
     * @param charName the characters name
     * @return GroupDTO
     */
    @RequestMapping(value = "/comic", method = RequestMethod.GET)
    public ResponseEntity<List<Comic>> getComicsByCharacterName(@RequestParam(value = "charName") String charName)
            throws Exception {

        Optional.of(charName).orElseThrow(IllegalArgumentException::new);

        List<Comic> comics = marvelService.getComicsByCharacter(charName).getData().getResults();

        return new ResponseEntity<>(comics, HttpStatus.OK);
    }
}
