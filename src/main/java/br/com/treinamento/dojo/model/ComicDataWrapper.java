package br.com.treinamento.dojo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by andreteodoro on 11/03/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ComicDataWrapper {

    private int code;
    private String status;
    private DataContainer<Comic> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DataContainer<Comic> getData() {
        return data;
    }

    public void setData(DataContainer<Comic> data) {
        this.data = data;
    }
}
