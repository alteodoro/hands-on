package br.com.treinamento.dojo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by andreteodoro on 11/03/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Comic {

    private int id;
    private String title;
    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
