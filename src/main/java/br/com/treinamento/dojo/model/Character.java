package br.com.treinamento.dojo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by andreteodoro on 11/03/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Character {

    private int id;
    private String name;
    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
