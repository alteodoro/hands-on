package br.com.treinamento.dojo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by andreteodoro on 11/03/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataContainer<E> {

    private int count;
    private List<E> results;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<E> getResults() {
        return results;
    }

    public void setResults(List<E> results) {
        this.results = results;
    }
}
