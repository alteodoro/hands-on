package br.com.treinamento.dojo.service;

import br.com.treinamento.dojo.model.CharacterDataWrapper;
import br.com.treinamento.dojo.model.ComicDataWrapper;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.net.URLEncoder;

/**
 * Created by andreteodoro on 11/03/17.
 */
@Component
public class MarvelService {

    private static final String MARVEL_URL = "https://gateway.marvel.com:443/v1/public/";
    private static final String CHARACTER_PATH = "characters";
    private static final String COMICS_PATH = "comics";

    @Autowired
    private RestClientService marvelApi;

    /**
     * Fetches the comics by the characters name
     *
     * @param charName The character name
     * @return String JSON
     * @throws Exception
     */
    @Cacheable(cacheNames="comics", key="#charName")
    public ComicDataWrapper getComicsByCharacter(String charName) throws Exception {
        CharacterDataWrapper character = new Gson().fromJson(getCharacter(charName), CharacterDataWrapper.class);
        ComicDataWrapper comics = null;

        if (character.getData().getResults().get(0) != null) {
            int charId = character.getData().getResults().get(0).getId();

            comics = new Gson().fromJson(getComicsByCharacterId(charId), ComicDataWrapper.class);
        }
        return comics;
    }

    /**
     * Fetches the list characters by name
     *
     * @param name The character name
     * @return String JSON
     * @throws Exception
     */
    private String getCharacter(String name) throws Exception {
        StringBuilder url = new StringBuilder();
        url.append(MARVEL_URL)
                .append(CHARACTER_PATH)
                .append("?name=")
                .append(URLEncoder.encode(name, "UTF-8"));

        return marvelApi.sendRequest(url.toString());
    }

    /**
     * Fetches the list of comics by a character
     *
     * @param charId The character id.
     * @return String JSON
     * @throws Exception
     */
    private String getComicsByCharacterId(int charId) throws Exception {
        StringBuilder url = new StringBuilder();
        url.append(MARVEL_URL)
                .append(CHARACTER_PATH)
                .append("/" + charId + "/")
                .append(COMICS_PATH)
                .append("?orderBy=-onsaleDate");

        return marvelApi.sendRequest(url.toString());
    }
}
