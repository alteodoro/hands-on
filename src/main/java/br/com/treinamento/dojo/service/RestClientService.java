package br.com.treinamento.dojo.service;

import org.glassfish.jersey.client.ClientConfig;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by andreteodoro on 11/03/17.
 */
@Component
public class RestClientService {

    private static final String PUBLIC_KEY = "ea979c6b4fad7cd86ad1173dc9cf400d";
    private static final String PRIVATE_KEY = "4b4e223ab336ca9d78bfc6d975f237c185287d79";

    public String sendRequest(String url) throws Exception {
        try {
            Timestamp ts = new Timestamp(new Date().getTime());
            Client client = ClientBuilder.newClient(new ClientConfig());

            String response = client.target(url)
                    .queryParam("apikey", PUBLIC_KEY)
                    .queryParam("ts", ts)
                    .queryParam("hash", generateHash(ts))
                    .request("application/json").get(String.class);

            return response;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private String generateHash(Timestamp ts) throws Exception {
        String hash = ts + PRIVATE_KEY + PUBLIC_KEY;

        return DigestUtils.md5DigestAsHex(hash.getBytes()).toString();
    }
}
