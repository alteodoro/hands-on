package br.com.treinamento.dojo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;

/**
 * Created by andreteodoro on 11/03/17.
 */
@Component
public class CacheService {

    @Autowired
    private CacheManager cacheManager;

    /**
     * List all cached data so far
     *
     * */
    public Object getCacheData() {
        Cache cache = cacheManager.getCache("comics");
        return cache.getNativeCache();
    }
}
